﻿#include "pch.h"
#include "OcvTestClass.h"

#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace CVWin_UWP_WRCLib1;
using namespace Platform;

using namespace cv;
using namespace std;


OcvTestClass::OcvTestClass()
{
}

int OcvTestClass::testOpenCV()
{
    Mat img(200, 400, CV_8UC3, Scalar(255, 0, 0));
    // cout << img << endl;

    return img.rows;
}