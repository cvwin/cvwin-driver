﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

using namespace CVWin_Driver_UWP3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
    InitializeComponent();


    //Mat img(200, 400, CV_8UC3, Scalar(255, 0, 0));
    //cout << img << endl;


    cv::namedWindow("Test Window", CV_WINDOW_AUTOSIZE);

    string imageName("Assets/HappyFish.jpg");

    Mat image = imread(imageName.c_str(), -1);
    if (image.empty()) {

        Mat img(200, 400, CV_8UC3, Scalar(255, 0, 0));
        imshow("Test Window", img);
        waitKey(0);

        return;
    }


    imshow("Test Window", image);
    waitKey(0);


}
