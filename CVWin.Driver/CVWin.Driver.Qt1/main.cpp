#include "mymainclass.h"
#include <QtWidgets/QApplication>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MyMainClass w;
	w.show();


	// testing....
	string imageName("../../sample-data/HappyFish.jpg");

	Mat image = imread(imageName, IMREAD_COLOR);

												 
												 
	if (!image.data)
	{
		cout << "Could not open or find the image" << std::endl;
	} 
	else
	{
		namedWindow("Display window", WINDOW_AUTOSIZE);
		imshow("Display window", image);

		waitKey(10);
	}
	// testing....


	return a.exec();
}
