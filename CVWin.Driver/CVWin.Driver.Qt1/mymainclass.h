#ifndef MYMAINCLASS_H
#define MYMAINCLASS_H

#include <QtWidgets/QMainWindow>
#include "ui_mymainclass.h"

class MyMainClass : public QMainWindow
{
	Q_OBJECT

public:
	MyMainClass(QWidget *parent = 0);
	~MyMainClass();

private:
	Ui::MyMainClassClass ui;
};

#endif // MYMAINCLASS_H
